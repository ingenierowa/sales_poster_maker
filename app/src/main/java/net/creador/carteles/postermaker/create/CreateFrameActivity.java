package net.creador.carteles.postermaker.create;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;

import android.view.GestureDetector;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import androidx.viewpager.widget.ViewPager;

import com.astuetz.PagerSlidingTabStrip;
import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;
import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBarWrapper;

import java.io.File;
import java.util.ArrayList;

import net.creador.carteles.postermaker.R;
import net.creador.carteles.postermaker.adapter.StickerViewPagerAdapter;
import uz.shift.colorpicker.LineColorPicker;

public class CreateFrameActivity extends Activity {
    private static final int SELECT_PICTURE_FROM_CAMERA = 9062;
    private static final int SELECT_PICTURE_FROM_GALLERY = 9072;
    protected static final String TAG = "ShiftPicker";
    private static final int TEXT_ACTIVITY = 9082;
    private static final int TYPE_SHAPE = 9062;
    private static final int TYPE_STICKER = 9072;
    public static Bitmap bitmapOriginal;
    public static Activity f6c;
    public static boolean isUpdated = false;
    static String mDrawableName = "";
    private final int OPEN_CUSTOM_ACITIVITY = 4;
    private StickerViewPagerAdapter _adapter;
    private ViewPager _mViewPager;
    int asuncCount = 0;
    private int backcolor = -1;
    private int bgColor = 0;
    private int[] bgId = new int[]{R.drawable.b1, R.drawable.b2, R.drawable.b3, R.drawable.b4, R.drawable.b5, R.drawable.b6,
            R.drawable.b7, R.drawable.b8, R.drawable.b9, R.drawable.b10, R.drawable.b11, R.drawable.b12, R.drawable.b13,
            R.drawable.b14, R.drawable.b15, R.drawable.b16, R.drawable.b17, R.drawable.b18, R.drawable.b19, R.drawable.b20,
            R.drawable.b21, R.drawable.b22, R.drawable.b23, R.drawable.b24, R.drawable.b25, R.drawable.b26,
            R.drawable.b27, R.drawable.b28, R.drawable.b29, R.drawable.b30, R.drawable.b31, R.drawable.b32, R.drawable.b33,
            R.drawable.b34, R.drawable.b35, R.drawable.b36, R.drawable.b37, R.drawable.b38, R.drawable.b39, R.drawable.b40,
            R.drawable.b41, R.drawable.b42, R.drawable.b43, R.drawable.b44, R.drawable.b45, R.drawable.b46, R.drawable.b47,
            R.drawable.b48, R.drawable.b49, R.drawable.b50, R.drawable.b51, R.drawable.b52, R.drawable.b53,
            R.drawable.b54, R.drawable.b55, R.drawable.b56, R.drawable.b57, R.drawable.b58,
            R.drawable.b59, R.drawable.b60, R.drawable.b61,
            R.drawable.b62,
            R.drawable.b63,
            R.drawable.b64,
            R.drawable.b65,
            R.drawable.b66,
            R.drawable.b67,
            R.drawable.b68,
            R.drawable.b69,
            R.drawable.b70,
            R.drawable.b71,
            R.drawable.b72,
            R.drawable.b73,
            R.drawable.b74,
            R.drawable.b75,
            R.drawable.b76,
            R.drawable.b77,
            R.drawable.b78,
            R.drawable.b79,
            R.drawable.b80,
            R.drawable.b81,
            R.drawable.b82,
            R.drawable.b83,
            R.drawable.b84,
            R.drawable.b85,
            R.drawable.b86,
            R.drawable.b87,
            R.drawable.b88,
            R.drawable.b89,
            R.drawable.b90,
            R.drawable.b91,
            R.drawable.b92,
            R.drawable.b93,
            R.drawable.b94,
            R.drawable.b95,
            R.drawable.b96,
            R.drawable.b97,
            R.drawable.b98,
            R.drawable.b99,
            R.drawable.b100,
            R.drawable.b101,
            R.drawable.b102,
            R.drawable.b103,
            R.drawable.b104,
            R.drawable.b105,
            R.drawable.b106,
            R.drawable.b107,
            R.drawable.b108,
            R.drawable.b109,
            R.drawable.b110,
            R.drawable.b111,
            R.drawable.b112,
            R.drawable.b113,
            R.drawable.b114,
            R.drawable.b115,
            R.drawable.b116,
            R.drawable.b117,
            R.drawable.b118,
            R.drawable.b119,
            R.drawable.b120,
            R.drawable.b121,
            R.drawable.b122,
            R.drawable.b123,
            R.drawable.b124,
            R.drawable.b125,
            R.drawable.b126,
            R.drawable.b127,
            R.drawable.b128,
            R.drawable.b129,
            R.drawable.b130,
            R.drawable.b131,
            R.drawable.b132,
            R.drawable.b133,
            R.drawable.b134,
            R.drawable.b135,
            R.drawable.b136,
            R.drawable.b137,
            R.drawable.b138,
            R.drawable.b139,
            R.drawable.b140,
            R.drawable.b141,
            R.drawable.b142,
            R.drawable.b143,
            R.drawable.b144,
            R.drawable.b145,
            R.drawable.b146,
            R.drawable.b147,
            R.drawable.b148,
            R.drawable.b149,
            R.drawable.b150,
            R.drawable.b151,
            R.drawable.b152,
            R.drawable.b153,
            R.drawable.b154,
            R.drawable.b155,
            R.drawable.b156,
            R.drawable.b157,
            R.drawable.b158,
            R.drawable.b159,
            R.drawable.b160,
            R.drawable.b161,
            R.drawable.b162,
            R.drawable.b163,
            R.drawable.b164,
            R.drawable.b165,
            R.drawable.b166,
            R.drawable.b167,
            R.drawable.b168,
            R.drawable.b169,
            R.drawable.b170,
            R.drawable.b171,
            R.drawable.b172,
            R.drawable.b173,
            R.drawable.b174,
            R.drawable.b175,
            R.drawable.b176,
            R.drawable.b177,
            R.drawable.b178,
            R.drawable.b179,
            R.drawable.b180,
            R.drawable.b181,
            R.drawable.b182,
            R.drawable.b183,
            R.drawable.b184,
            R.drawable.b185,
            R.drawable.b186,
            R.drawable.b187,
            R.drawable.b188,
            R.drawable.b189,
            R.drawable.b190,
            R.drawable.b191,
            R.drawable.b192,
            R.drawable.b193,
            R.drawable.b194,
            R.drawable.b195,
            R.drawable.b196,
            R.drawable.b197,
            R.drawable.b198,
            R.drawable.b199,
            R.drawable.b200,
            R.drawable.b201,
            R.drawable.b202,
            R.drawable.b203,
            R.drawable.b204,
            R.drawable.b205,
            R.drawable.b206,
            R.drawable.b207,
            R.drawable.b208,
            R.drawable.b209,
            R.drawable.b210,
            R.drawable.b211,
            R.drawable.b212,
            R.drawable.b213,
            R.drawable.b214,
            R.drawable.b215,
            R.drawable.b216,
            R.drawable.b217,
            R.drawable.b218,
            R.drawable.b219,
            R.drawable.b220,
            R.drawable.b221,
            R.drawable.b222,
            R.drawable.b223,
            R.drawable.b224,
            R.drawable.b225,
            R.drawable.b226,
            R.drawable.b227,
            R.drawable.b228,
            R.drawable.b229,
            R.drawable.b230,
            R.drawable.b231,
            R.drawable.b232,
            R.drawable.b233,
            R.drawable.b234,
            R.drawable.b235,
            R.drawable.b236,
            R.drawable.b237,
            R.drawable.b238,
            R.drawable.b239,
            R.drawable.b240,
            R.drawable.b241,
            R.drawable.b242,
            R.drawable.b243,
            R.drawable.b244,
            R.drawable.b245,
            R.drawable.b246,
            R.drawable.b247,
            R.drawable.b248,
            R.drawable.b249,
            R.drawable.b250,
            R.drawable.b251,
            R.drawable.b252,
            R.drawable.b253,
            R.drawable.b254,
            R.drawable.b255,
            R.drawable.b256,
            R.drawable.b257,
            R.drawable.b258,
            R.drawable.b259,
            R.drawable.b260,
            R.drawable.b261,
            R.drawable.b262,
            R.drawable.b263,
            R.drawable.b264,
            R.drawable.b265,
            R.drawable.b266,
            R.drawable.b267,
    };
    private RelativeLayout bg_container;
    private RelativeLayout bg_list_container;
    private Bitmap bitmap;
    LinearLayout bottom;
    Button btn_copy;
    private ImageButton btn_up_down;
    private RelativeLayout camgal_container;
    private RelativeLayout color_list_container;
    private int curTileId = 0;
    private HorizontalScrollView decorative_shape;
    private boolean editMode = false;
    Editor editor;
    private File f7f = null;
    private HorizontalScrollView flower_shape;
    private View focusedView;
    String frame_Name;
    private ImageView frame_img;
    GestureDetector gd = null;
    private ImageView guideline;
    private int height;
    private LineColorPicker horizontalPicker;
    ImageSdAdapter imageListAdapter;
    private RelativeLayout image_list_container;
    HorizontalListView images_listview;
    RelativeLayout lay_sticker;
    private HorizontalScrollView leaves_shape;
    private RelativeLayout main_rel;
    private Bitmap mask;
    private Bitmap newBit;
    String[] pallete = new String[]{"#4182b6", "#4149b6", "#7641b6", "#b741a7", "#c54657", "#d1694a", "#24352a", "#b8c847", "#67bb43", "#41b691", "#293b2f", "#1c0101", "#420a09", "#b4794b", "#4b86b4", "#93b6d2", "#72aa52", "#67aa59", "#fa7916", "#16a1fa", "#165efa", "#1697fa"};
    String[] pallete1 = new String[]{"#b8c847", "#67bb43", "#41b691", "#4182b6", "#4149b6", "#7641b6", "#b741a7", "#c54657", "#d1694a", "#26ffd5", "#7b5b37", "#e7ae95", "#874848", "#987064", "#daa18f", "#deb289", "#e24d4e", "#768282", "#333c6f", "#c9b82b", "#1de592", "#156526", "#fedd31", "#f1ec12", "#806d88", "#055c5a", "#012153", "#e1e3ea", "#012153", "#00abff", "#b2c9ab", "#6fae93"};
    private ProgressDialog pdia;
    SharedPreferences prefs;
    private int processs;
    String profile_type;
    private ImageButton ratio;
    String ratioStr = "1:1";
    private RelativeLayout ratio_container;
    private RelativeLayout rel;
    private RelativeLayout res_container;
    private Animation scaleAnim;
    private VerticalSeekBarWrapper seekBarContainer;
    private int seekValue = 90;
    private RelativeLayout shape_container;
    private RelativeLayout shape_rel;
    int size_list;
    private Animation slideDown;
    private Animation slideUp;
    ArrayList<String> stkrNameList;
    private RelativeLayout tabbasic;
    private RelativeLayout tabbg;
    private RelativeLayout tabcamgal;
    private RelativeLayout tabcolor;
    private RelativeLayout tabdecorative;
    private RelativeLayout tabflower;
    private RelativeLayout tabimages;
    private RelativeLayout tableaves;
    PagerSlidingTabStrip tabs;
    private RelativeLayout tabtexture;
    String temp_path = "";
    private int[] textureId = new int[]{R.drawable.t1, R.drawable.t2, R.drawable.t3, R.drawable.t4, R.drawable.t5, R.drawable.t6, R.drawable.t7, R.drawable.t8, R.drawable.t9, R.drawable.t10, R.drawable.t11, R.drawable.t12, R.drawable.t13, R.drawable.t14, R.drawable.t15, R.drawable.t16, R.drawable.t17, R.drawable.t18, R.drawable.t19, R.drawable.t20, R.drawable.t21, R.drawable.t22, R.drawable.t23, R.drawable.t24, R.drawable.t25, R.drawable.t26, R.drawable.t27, R.drawable.t28, R.drawable.t29, R.drawable.t30, R.drawable.t31, R.drawable.t32, R.drawable.t33, R.drawable.t34, R.drawable.t35, R.drawable.t36, R.drawable.t37, R.drawable.t38, R.drawable.t39, R.drawable.t40, R.drawable.t41, R.drawable.t42, R.drawable.t43, R.drawable.t44, R.drawable.t45, R.drawable.t46, R.drawable.t47, R.drawable.t48, R.drawable.t49, R.drawable.t50, R.drawable.t51, R.drawable.t52, R.drawable.t53, R.drawable.t54, R.drawable.t55, R.drawable.t56, R.drawable.t57, R.drawable.t58, R.drawable.t59, R.drawable.t60};
    private RelativeLayout texture_list_container;
    ArrayList<Bitmap> thumbnails = new ArrayList();
    private VerticalSeekBar tile_seekbar;
    private Typeface ttf;
    private RelativeLayout txt_stkr_rel;
    ArrayList<String> uri = new ArrayList();
    public SeekBar verticalSeekBar = null;
    private int width;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().setFlags(131072, 131072);
        setContentView(R.layout.activity_create_frame);
    }
}
