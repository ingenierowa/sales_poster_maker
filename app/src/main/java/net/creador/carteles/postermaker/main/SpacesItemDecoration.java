package net.creador.carteles.postermaker.main;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;


public class SpacesItemDecoration
        extends RecyclerView.ItemDecoration
{
    private int space;

    public SpacesItemDecoration(int paramInt)
    {
        this.space = paramInt;
    }

    public void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
    {
        paramRect.left = this.space;
        paramRect.right = this.space;
        paramRect.bottom = this.space;
        paramRect.top = this.space;
    }
}

