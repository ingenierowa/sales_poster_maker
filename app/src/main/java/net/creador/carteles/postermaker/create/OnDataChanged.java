package net.creador.carteles.postermaker.create;

public interface OnDataChanged {
    void updateAdapter();

    void updateInstLay(boolean z);
}
