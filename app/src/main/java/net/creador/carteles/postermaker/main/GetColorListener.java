package net.creador.carteles.postermaker.main;

public interface GetColorListener {
    void onColor(int i, String str);
}
