package net.creador.carteles.postermaker.main;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;



import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import net.creador.carteles.postermaker.BuildConfig;
import net.creador.carteles.postermaker.R;

public class MyWorkActivity extends Activity {

    RecyclerView recycleImages;
    TextView no_data;
    String filepath = "";
    ImageView imageBack;
    LinearLayout linearAdsBanner;
    AdView mAdView;

    InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_work);

        linearAdsBanner = findViewById(R.id.linearAds);
        addBannnerAds();
        loadInterstitialAdvertise();

        recycleImages = findViewById(R.id.recycleImages);
        imageBack = findViewById(R.id.imageBack);
        no_data = findViewById(R.id.no_data);

        GridLayoutManager localGridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recycleImages.setLayoutManager(localGridLayoutManager);
        recycleImages.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen._1sdp)));

        filepath = Constant.getFilePath(getApplicationContext());
        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    public void loadInterstitialAdvertise() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle(getString(R.string.loading_ad));
        progress.setCancelable(false);
        progress.show();


        mInterstitialAd.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if(mInterstitialAd.isLoaded()) {
                    progress.dismiss();
                    mInterstitialAd.show();
                } else {
                    progress.dismiss();
                }
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                progress.dismiss();
            }

        });
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }


    void loadFullScreenAds() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));
        mInterstitialAd.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if(mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }

    void addBannnerAds() {
        if (Constant.isNetworkConnected(MyWorkActivity.this)) {
            mAdView = new AdView(MyWorkActivity.this);
            mAdView.setAdSize(AdSize.SMART_BANNER);
            mAdView.setAdUnitId(getString(R.string.id_b_g));
            AdRequest adRequest = new AdRequest.Builder().build();
            linearAdsBanner.addView(mAdView);
            mAdView.loadAd(adRequest);
        }
    }



    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getImageData();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }

        super.onDestroy();
    }

    private void getImageData() {
        if (loadImageList() != null && loadImageList().size() == 0) {
            no_data.setVisibility(View.VISIBLE);
        }
        GalleyAdapter galleyAdapter = new GalleyAdapter(loadImageList());
        recycleImages.setAdapter(galleyAdapter);
    }

    private ArrayList<String> loadImageList() {
        ArrayList<String> stringList;
        File file = new File(filepath);
        if (file.exists()) {
            stringList = new ArrayList<String>(Arrays.asList(file.list()));
            return stringList;
        }
        return null;
    }

    private class GalleyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        ArrayList<String> filePathList;

        private GalleyAdapter(ArrayList<String> filePathList) {
            this.filePathList = filePathList;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ImageHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.image_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

            ImageHolder imageHolder = (ImageHolder) holder;
            Glide.with(MyWorkActivity.this).load("file://" + filepath + "/" + filePathList.get(position)).into(imageHolder.img_display);

        }

        @Override
        public int getItemCount() {
            return filePathList.size();
        }

        private class ImageHolder extends RecyclerView.ViewHolder {
            ImageView img_display, imgDelete, imgShare;

            private ImageHolder(View itemView) {
                super(itemView);
                img_display = itemView.findViewById(R.id.imageView);
                imgDelete = itemView.findViewById(R.id.imgDelete);
                imgShare = itemView.findViewById(R.id.imgShare);
                imgDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(MyWorkActivity.this)
                                .setTitle(getString(R.string.delete_title))
                                .setMessage(getString(R.string.are_sure))
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        File file = new File(filepath + "/" + filePathList.get(getAdapterPosition()));
                                        if (file.delete()) {
                                            getImageData();
                                            notifyDataSetChanged();
                                            Toast.makeText(MyWorkActivity.this, getString(R.string.deleted_ok), Toast.LENGTH_SHORT).show();
                                            MediaScannerConnection.scanFile(getApplicationContext(), new String[]{file.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                                                public void onScanCompleted(String string, Uri uri) {
                                                }
                                            });

                                        }
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                }).show();
                    }
                });
                imgShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        shareImg(filepath + "/" + filePathList.get(getAdapterPosition()));
                    }
                });



            }
        }



        private void shareImg(String compoundPath) {
            String body = getString(R.string.share) ;
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);

            File file = new File(compoundPath);
            Uri uriImage = null;
            if (Build.VERSION.SDK_INT >= 26) {
                uriImage = FileProvider.getUriForFile(MyWorkActivity.this, BuildConfig.APPLICATION_ID + ".fileprovider",file);
            } else {
                uriImage = Uri.fromFile(file);
            }
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_STREAM, uriImage);
            intent.putExtra(Intent.EXTRA_TEXT, body);
            startActivity(Intent.createChooser(intent, getString(R.string.share)));

        }

    }
}

