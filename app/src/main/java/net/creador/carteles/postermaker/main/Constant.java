package net.creador.carteles.postermaker.main;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Environment;

import java.io.File;

public class Constant {

    private static File dir;
    private static File filepath;

    static String getFilePath(Context paramContext) {
        filepath = Environment.getExternalStorageDirectory();
        dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/Poster Maker");

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir.getAbsolutePath();
    }

    static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        return cm.getActiveNetworkInfo() != null;
    }
}
